<?php
/**
 * Internationalisation for PostEdit extension
 *
 * @file
 * @ingroup Extensions
 */

$messages = array();

/** English
 * @author Ori.livneh
 */
$messages['en'] = array(
	'wikidatapostedit' => 'WikidataPostEdit',
	'wikidata-postedit-desc' => 'Shows notification after edit',
    'wikidata-postedit-notification_1' => '<br/>please give feedback',
);


