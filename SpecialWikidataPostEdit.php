<?php

class AllPagesPager extends TablePager {

    function getQueryInfo() {
        return array(
            'tables' => 'wikidataposteditstats',
            'fields' => array('type', 'counting'),
            'conds' => ''
        );
    }

    function isFieldSortable($field) {
        return true;
    }

    function getDefaultSort() {
        return 'type';
    }

    function formatValue($name, $value) {
        return $value;
    }

    function getFieldNames() {
        return array(
            'type'       => 'type',
            'counting'    => 'count',
        );
    }
}



class SpecialWikidataPostEdit extends SpecialPage {
    function __construct() {
        parent::__construct('WikidataPostEdit');
    }

    function execute($par) {
        $request = $this->getRequest();
        $output = $this->getOutput();
        $this->setHeaders();

        /* handle the POST-Request  */
        if ($request->getMethod() == "POST") {

            /* check if the type is valid */
            $type = $request->getVal('type');
            if($type != 'static' && $type != 'dynamic'){
                echo "I can't do this Dave! "+$type;
                exit();
            }

            /* update the db */
            $dbw = wfGetDB(DB_MASTER);
            $dbw->update( 'wikidataposteditstats', array( 'counting=counting+1' ), array( 'type' => $type ), __METHOD__ );

            echo "done";

            /*end POST*/
        } else {
            /* GET */
            /* print the table from the database */
            $pager = new AllPagesPager();
            $output->addHTML(
                $pager->getNavigationBar() . '<ol>' .
                    $pager->getBody() . '</ol>' .
                    $pager->getNavigationBar()
            );
        }
    }
}
