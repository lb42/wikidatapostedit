<?php
/**
 * WikidataPostEdit Extension for MediaWiki.
 *
 * @file
 * @ingroup Extensions
 *
 * @license GPL v2 or later
 * @version 0.2
 */

$wgExtensionCredits[ 'other' ][] = array(
    'path' => __FILE__,
    'name' => 'WikidataPostedit',
    'version' => '0.00001',
    'url' => 'TODO',
    'author' => array('LB',),
    'descriptionmsg' => 'wikidata-postedit-desc'
);



// Register files
$wgExtensionMessagesFiles[ 'WikidataPostEdit' ] = __DIR__ . '/WikidataPostEdit.i18n.php';
$wgAutoloadClasses['WikidataPostEditHooks'] = __DIR__ . '/WikidataPostEdit.hooks.php';


// Register modules
$wgResourceModules[ 'ext.wikidataPostEdit' ] = array(
	'scripts' => 'resources/ext.wikidataPostEdit.js',
	'styles' => 'resources/ext.wikidataPostEdit.css',
	'messages' => array(
		'wikidata-postedit-desc',
        'wikidata-postedit-notification_1',
	),
	'position' => 'top',
	'localBasePath' => __DIR__,
	'remoteExtPath' => 'WikidataPostEdit',
);


# Schema updates for update.php
$wgHooks['LoadExtensionSchemaUpdates'][] = 'createWikidataPostEditTable';
function createWikidataPostEditTable(DatabaseUpdater $updater) {
    $updater->addExtensionTable('wikidatapostedit',
        dirname(__FILE__) . '/table.sql', true);
    return true;
}

// Register hooks
$wgHooks['BeforePageDisplay'][] = 'WikidataPostEditHooks::onBeforePageDisplay';


// SpecialPage
$wgAutoloadClasses['SpecialWikidataPostEdit'] = __DIR__ . '/SpecialWikidataPostEdit.php'; # Location of the SpecialUIFeedback class (Tell MediaWiki to load this file)
$wgSpecialPages['WikidataPostEdit'] = 'SpecialWikidataPostEdit'; # Tell MediaWiki about the new special page and its class name
