<?php

class WikidataPostEditHooks {

    public static function onBeforePageDisplay(&$out) {
        $out->addModules('ext.wikidataPostEdit');
        return true;
    }


}
