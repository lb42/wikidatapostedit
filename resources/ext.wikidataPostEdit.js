/*jshint browser: true */
/*global mediaWiki, jQuery */
( function ( mw, $ ) {
	'use strict';


	

	function showNotification(message, timeout, offset_top){


		


		if( timeout === null ){
			timeout = 5000;
		}

		/* create the notification-popup */
		var notification = document.createElement('div');
		notification.setAttribute('class', 'wikidata-postedit-notification');
		notification.innerHTML = message;

		
		$(notification).click(function() {
			console.log('hide notification');
			$(this).animate({"right": "-500px"},500);
		});

		$(notification).hover(function(){
			window.clearTimeout(timer);
		});

		if( offset_top !== null ){
			$(notification).css('top',offset_top);
		}
		$('body').prepend(notification);

		$(notification).animate({"right": "+=50px"},500);

		/* set a timer to hide the popup */
		var timer = window.setTimeout(function(){
			$(notification).animate(
				{"right": "-500px"}, /* move the popup out of sight*/
				500, /* in 500ms */
				function() {$(notification).remove();} /* remove from the DOM after that*/
			);
		},timeout);

	}


	$( document ).ready( function () {
		try {

			$( wb ).on( 'stopItemPageEditMode', function( a, b) {
				// console.log(b['API_VALUE_KEY']);
				// console.log(b['__toolbarParent'][0]);

				var offset_top = 100;
				try{
					offset_top = $(b.__toolbarParent[0]).offset().top;
				}catch(e){
					console.log('cant get parent-object');
					console.log(b);
				}

				showNotification( ''+b.API_VALUE_KEY+'<br/><br/>'+mw.message('wikidata-postedit-notification_1', mw.user), 5000, offset_top );

				console.log(mw.util.wikiGetlink('Special:WikidataPostEdit'));
				$.post(mw.util.wikiGetlink('Special:WikidataPostEdit'), {'type':'dynamic'});
				// $.post(mw.util.wikiGetlink('Special:WikidataPostEdit'), data: {'type':'dynamic'} );

			} );

		} catch (e) {
			console.log('wikibase not found');
		}

	} );



}( mediaWiki, jQuery ) );
